# Send E-mail

This is a simple script to send e-mails via the [Ethereal service](https://ethereal.email/).

## Starting the server

```
npm install
npm start
```
import nodemailer from 'nodemailer';

export function sendemail(remetente, destinatario, titulo, content) {

    nodemailer.createTestAccount((err, account) => {
        let transporter = nodemailer.createTransport({
            host: 'smtp.ethereal.email',
            port: 587,
            secure: false,
            auth: {
                user: account.user,
                pass: account.pass
            }
        });

        let mailOptions = {
            from: `"CVM Jud Mock" <${remetente}>`,
            to: destinatario,
            subject: titulo,
            text: content,
            html: content
        };

        transporter.sendMail(mailOptions, (error, info) => {
            if (error) { return console.log(error); }
            console.log('****');
            console.log(titulo);
            console.log(nodemailer.getTestMessageUrl(info));
            console.log('****');
        });
    });
}